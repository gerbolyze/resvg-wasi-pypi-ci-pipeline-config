#!/bin/sh

set -e

if test $# -ge 1; then
    ln -s "$1" "$(dirname "$0")/resvg" || echo "Using existing resvg dir"
fi
cd "$(dirname $0)"
BASEDIR="$(realpath .)"

cd resvg
cargo build --target wasm32-wasi --no-default-features --features 'text,system-fonts,filter' --release --bin resvg
cd usvg
cargo build --target wasm32-wasi --no-default-features --features 'text,system-fonts' --release --bin usvg

cd "$BASEDIR"
cp resvg/target/wasm32-wasi/release/{usvg,resvg}.wasm "resvg_wasi/"
python3 setup.py bdist_wheel

