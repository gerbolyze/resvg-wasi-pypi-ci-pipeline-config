import subprocess
from setuptools import setup, find_packages
from pathlib import Path
import re
import shutil

resvg_path = (Path(__file__).parent / 'resvg').absolute()
package_path = (Path(__file__).parent / 'resvg_wasi').absolute()

def version():
    """ Take upstream resvg version and append patch level suffix if CI config repo has patch tag.
    """
    # Upstream repo either has upstream tag "v0.17.0" or fork tag "fork-v0.17.0" in case we apply a local patch to
    # upstream sources
    try:
        res = subprocess.run(['git', '-C', resvg_path, 'describe', '--tags', '--match', 'v*', '--match', 'fork-v*'],
                capture_output=True, check=True, text=True)
    except:
        subprocess.run(['git', '-C', resvg_path, 'describe', '--tags', '--match', 'v*', '--match', 'fork-v*'], check=True)
    resvg_match = re.match(r'^(fork-)?v?([0-9]+\.[0-9]+\.[0-9]+)$', res.stdout.strip())
    if not resvg_match:
        raise ValueError('Cannot determine resvg version to compute package version')
    version = resvg_match.group(2)

    # CI config repo may have tag like "patch-v0.17.0-2" in case we apply a patch to the CI config/pypi wrapper sources
    res = subprocess.run(['git', '-C', package_path, 'describe', '--tags', '--match', 'patch-v*'],
            capture_output=True, text=True)
    package_match = re.match(r'^patch-v([0-9]+\.[0-9]+\.[0-9]+)(-[0-9]+)$', res.stdout.strip())
    if res.returncode == 0 and package_match:
        if package_match.group(1) == version:
            version += package_match.group(2)

    # Result usually is upstream version like "0.17.0", or in case of CI config/pypi wrapper patches "0.17.0-1", -2 etc.
    return version

def long_description():
    with open("README.md") as f:
        return f.read()

setup(
    name="resvg-wasi",
    version=version(),
    author="jaseg",
    author_email="pypi@jaseg.de",
    description="resvg SVG renderer and normalizer",
    long_description=long_description(),
    long_description_content_type="text/markdown",
    license="MPLv2.0", # same as resvg
    python_requires="~=3.7",
    setup_requires=["wheel"],
    install_requires=[
        "importlib_resources; python_version<'3.9'",
        "appdirs~=1.4",
        "wasmtime>=0.28",
        "click >= 4.0"
    ],
    packages=["resvg_wasi"],
    package_data={"resvg_wasi": [
        "*.wasm",
    ]},
    entry_points={
        "console_scripts": [
            "wasi-resvg = resvg_wasi:run_resvg",
            "wasi-usvg = resvg_wasi:run_usvg",
        ],
    },
    project_urls={
        "Source Code": "https://git.jaseg.de/resvg-wasi",
        "Bug Tracker": "https://github.com/jaseg/resvg-wasi/issues",
    },
    classifiers=[
        "License :: OSI Approved :: Mozilla Public License 2.0 (MPL 2.0)",
    ],
)
